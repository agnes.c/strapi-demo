module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/panels/schema',
      handler: 'panel.schema',
    }
  ]
}
