'use strict';

/**
 * panel controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController(
  "api::panel.panel",
  ({ strapi }) => ({
    async schema(ctx) {
      const panelContentTypes = strapi.api.panel.contentTypes;
      const panelComponents = [];
      for(const component of panelContentTypes.panel.attributes.Children.components) {
        panelComponents.push(strapi.components[component]);
      }

      panelContentTypes.panel.attributes.Children.components = panelComponents;
      return panelContentTypes;
    },
  })
);
