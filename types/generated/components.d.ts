import type { Schema, Attribute } from '@strapi/strapi';

export interface ConditionLoginFormCondition extends Schema.Component {
  collectionName: 'components_condition_login_form_conditions';
  info: {
    displayName: 'Click';
    description: '';
  };
  attributes: {
    Result: Attribute.String;
  };
}

export interface ConditionSubmitButton extends Schema.Component {
  collectionName: 'components_condition_submit_buttons';
  info: {
    displayName: 'Show';
    description: '';
  };
  attributes: {
    FormPath: Attribute.String;
    Operator: Attribute.String;
    Value: Attribute.String;
  };
}

export interface FormLoginForm extends Schema.Component {
  collectionName: 'components_form_login_forms';
  info: {
    displayName: 'LoginForm';
    description: '';
  };
  attributes: {
    Input: Attribute.Component<'item.input', true>;
    Button: Attribute.Component<'item.button', true>;
  };
}

export interface FormUserRegistrationForm extends Schema.Component {
  collectionName: 'components_form_user_registration_forms';
  info: {
    displayName: 'UserRegistrationForm';
    description: '';
  };
  attributes: {
    Input: Attribute.Component<'item.input', true>;
  };
}

export interface ItemButton extends Schema.Component {
  collectionName: 'components_item_buttons';
  info: {
    displayName: 'Button';
  };
  attributes: {
    Label: Attribute.String;
    Variant: Attribute.String;
    CSSProp: Attribute.String;
    ShowButtonCondition: Attribute.Component<'condition.submit-button', true>;
    ClickButtonCondition: Attribute.Component<
      'condition.login-form-condition',
      true
    >;
  };
}

export interface ItemInput extends Schema.Component {
  collectionName: 'components_item_inputs';
  info: {
    displayName: 'Input';
    description: '';
  };
  attributes: {
    Type: Attribute.String;
    Label: Attribute.String;
    ComponentAPI: Attribute.JSON;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'condition.login-form-condition': ConditionLoginFormCondition;
      'condition.submit-button': ConditionSubmitButton;
      'form.login-form': FormLoginForm;
      'form.user-registration-form': FormUserRegistrationForm;
      'item.button': ItemButton;
      'item.input': ItemInput;
    }
  }
}
